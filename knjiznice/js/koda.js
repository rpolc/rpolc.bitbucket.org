
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";
  var ime, priimek, datumrojstva, spol, visina, teza, vpritisk, npritisk, utrip, alergije, temperatura;
  switch(stPacienta){
    case 1: //Janez Novak
      ime="Janez";
      priimek="Novak";
      datumrojstva="1978-12-6T18:24";
      spol="m";
      visina="192";
      teza="130";
      vpritisk="140";
      npritisk="96";
      utrip="85";
      alergije="";
      temperatura="37.0";
      document.getElementById("name1").innerHTML=ime;
      document.getElementById("sname1").innerHTML=priimek;
      document.getElementById("date1").innerHTML=datumrojstva;
      document.getElementById("ht1").innerHTML=visina;
      document.getElementById("wt1").innerHTML=teza;
      document.getElementById("hr1").innerHTML=utrip;
      document.getElementById("bp1").innerHTML=vpritisk+"/"+npritisk;
    case 2: //Mojca Kovač
      ime="Mojca";
      priimek="Kovač";
      datumrojstva="1994-5-30T09:30";
      spol="z";
      visina="165";
      teza="58";
      vpritisk="120";
      npritisk="80";
      utrip="68";
      alergije="";
      temperatura="36.2";
      document.getElementById("name2").innerHTML=ime;
  document.getElementById("sname2").innerHTML=priimek;
  document.getElementById("date2").innerHTML=datumrojstva;
  document.getElementById("ht2").innerHTML=visina;
  document.getElementById("wt2").innerHTML=teza;
  document.getElementById("hr2").innerHTML=utrip;
  document.getElementById("bp2").innerHTML=vpritisk+"/"+npritisk;
    case 3: //Luka Hrovat
      ime="Luka";
      priimek="Hrovat";
      datumrojstva="1990-8-13T15:12";
      spol="m";
      visina="178";
      teza="70";
      vpritisk="110";
      npritisk="68";
      utrip="72";
      alergije="jagode";
      temperatura="36.8";
      document.getElementById("name3").innerHTML=ime;
  document.getElementById("sname3").innerHTML=priimek;
  document.getElementById("date3").innerHTML=datumrojstva;
  document.getElementById("ht3").innerHTML=visina;
  document.getElementById("wt3").innerHTML=teza;
  document.getElementById("hr3").innerHTML=utrip;
  document.getElementById("bp3").innerHTML=vpritisk+"/"+npritisk;
  }
  /*if(stPacienta==1){
  document.getElementById("name1").innerHTML=ime;
  document.getElementById("sname1").innerHTML=priimek;
  document.getElementById("date1").innerHTML=datumrojstva;
  document.getElementById("ht1").innerHTML=visina;
  document.getElementById("wt1").innerHTML=teza;
  document.getElementById("hr1").innerHTML=utrip;
  document.getElementById("bp1").innerHTML=vpritisk+"/"+npritisk;
  }
   else if(stPacienta==2){
  document.getElementById("name2").innerHTML=ime;
  document.getElementById("sname2").innerHTML=priimek;
  document.getElementById("date2").innerHTML=datumrojstva;
  document.getElementById("ht2").innerHTML=visina;
  document.getElementById("wt2").innerHTML=teza;
  document.getElementById("hr2").innerHTML=utrip;
  document.getElementById("bp2").innerHTML=vpritisk+"/"+npritisk;
  }
  else if(stPacienta==3){
  document.getElementById("name3").innerHTML=ime;
  document.getElementById("sname3").innerHTML=priimek;
  document.getElementById("date3").innerHTML=datumrojstva;
  document.getElementById("ht3").innerHTML=visina;
  document.getElementById("wt3").innerHTML=teza;
  document.getElementById("hr3").innerHTML=utrip;
  document.getElementById("bp3").innerHTML=vpritisk+"/"+npritisk;
  }*/
  //generira podatke
  $.ajaxSetup({
    headers: {
        "Authorization": authorization,
        "Ehr-Session":getSessionId()
    }
  });
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function (data) {
        var ehrId = data.ehrId;
        $("#header").html("EHR: " + ehrId);

        var partyData = {
            firstNames: ime,
            lastNames: priimek,
            dateOfBirth: datumrojstva,
            partyAdditionalInfo: [{key: "ehrId",value: ehrId, 
              key:"vital_signs/body_temperature/any_event/temperature|magnitude", value:temperatura,
              key: "vital_signs/blood_pressure/any_event/systolic",value:vpritisk,
              key: "vital_signs/blood_pressure/any_event/diastolic",value:npritisk,
              key: "vital_signs/height_length/any_event/body_height_length",value:visina,
              key: "vital_signs/body_weight/any_event/body_weight",value:teza,
              key: "gender",value:spol}]
        };
        $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            success: function (party) {
                if (party.action == 'CREATE') {
                    console.log("yay %s", ehrId);
                    //vitalniZnaki(temperatura, vpritisk, npritisk, visina, teza, ehrId);
                    $('#seznamID').append("*"+ehrId+" "+visina+" cm "+teza+" kg. *");
                }
            }
           
        });
    }
  });
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
